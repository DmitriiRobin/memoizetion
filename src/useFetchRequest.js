import { useState, useEffect } from 'react';

export default function useFetchRequest(searchString, isSortingDesc) {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        console.count("render fetch");
        fetch("https://reqres.in/api/products")
          .then((response) => response.json())
          .then((json) =>
            setProducts(
              json.data
                .filter((item) => item.name.includes(searchString))
                .sort((a, z) =>
                  isSortingDesc
                    ? z.name.localeCompare(a.name)
                    : a.name.localeCompare(z.name)
                )
            )
          );
      }, [searchString, isSortingDesc]);

      return products;
}
