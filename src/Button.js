import React from 'react';

export default React.memo(function Button({ children, setSortingDesc }) {
    console.count("render button");
    return (
        <button
            onClick={() => setSortingDesc()}
            style={{ backgroundColor: "lightgray" }}
        >
            {children}
        </button>
    )
});