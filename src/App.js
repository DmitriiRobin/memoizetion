import { useCallback, useState } from "react";
import ListItem from './ListItem';
import Button from './Button';
import useFetchRequest from './useFetchRequest';
import "./App.css";

function App() {
  const [searchString, setSearchString] = useState("");
  const [isSortingDesc, setSortingDesc] = useState(false);
  const products = useFetchRequest(searchString, isSortingDesc)

  console.count("render app");
  
  const memoisedSetSearchString = useCallback(() => {
    setSortingDesc((value) => !value)
  }, [])

  return (
    <div className="App">
      <input
        type="search"
        value={searchString}
        onChange={(e) => setSearchString(e.target.value)}
      />
      <Button setSortingDesc={memoisedSetSearchString}>
        Change sort direction
      </Button>
      <ul>
        {products.map((product) => {
          return <ListItem key={product.id}>{product.name}</ListItem>;
        })}
      </ul>
    </div>
  );
}

export default App;
